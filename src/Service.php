<?php

namespace Sarbas\Package;

class Service
{
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function say(): string
    {
        return "Hello from project, my dear " . $this->name;
    }
}